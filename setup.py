import setuptools

# TODO: add the constraints module because of the arrow alignment in the plots
setuptools.setup(author="Matthias Feurer",
                 author_email="feurerm@informatik.uni-freiburg.de",
                 name="pyMetaLearn",
                 description="Metalearning utilities for python.",
                 version="0.1dev",
                 packages=setuptools.find_packages(),
                 package_data={'': ['*.txt', '*.md']},
                 install_requires=["ParamSklearn",
                                   "liac-arff",
                                   "numpy",
                                   "scipy",
                                   "lockfile",
                                   "pandas",
                                   "scikit-learn",
                                   "matplotlib",
                                   "HPOlib",],
                 license="BSD",
                 platforms=['Linux'],
                 classifiers=[],
                 url="github.com/mfeurer/pymetalearn")
