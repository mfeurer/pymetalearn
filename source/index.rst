.. pyMetaLearn documentation master file, created by
   sphinx-quickstart on Fri Aug 29 11:23:58 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pyMetaLearn's documentation!
=======================================

Contents:

.. toctree::
   :maxdepth: 2

   about
   installation
   workflows
   openml



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

