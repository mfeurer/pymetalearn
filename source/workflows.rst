.. _workflows:

===============
Basic Workflows
===============

This package comes with a bunch of scripts which include several useful
workflows:

1. :ref:`Download data from OpenML <workflows_001_openml>`
2. :ref:`Calculate Metafeatures <workflows_100_metafeatures>`
3. :ref:`Create HPOlib directories for grid search <workflows_200_gridsearch>`
4. :ref:`Create HPOlib directories for default configuration <workflows_201_default>`
5. :ref:`Create HPOlib directories for Meta-Learning <workflows_300_metalearning>`
6. :ref:`Plot the results <workflows_400_plotting>`

.. _workflows_001_openml:

Download data from OpenML
=========================

.. autofunction:: data.openml.download_tasks_and_datasets

.. autofunction:: data.openml.create_tasks

.. autofunction:: data.openml.create_split_files

.. _workflows_100_metafeatures:

Calculate Metafeatures
======================

.. _workflows_200_gridsearch:

Create HPOlib Directories for Grid Search
=========================================

.. _workflows_201_default:

Create HPOlib directories for default configuration
===================================================

.. _workflows_300_metalearning:

Create HPOlib directories for Meta-Learning
===========================================

.. _workflows_400_plotting:

Plot the Results
================
