.. _openml:

=============================
An OpenML connector in python
=============================

.. autoclass:: pyMetaLearn.data_repositories.openml.apiconnector.APIConnector