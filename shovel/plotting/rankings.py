from functools import reduce
import glob
import os

import numpy as np
import matplotlib.pyplot as plt

import pyMetaLearn.directory_manager
from pyMetaLearn.entities import entities_base
from pyMetaLearn.entities.task import Task as pyMetaLearnTask
from pyMetaLearn.utils import plot_utils as meta_plot_util

from HPOlib.Plotting import plot_util

from joblib import Memory, Parallel, delayed
from shovel import task as shovel_task


@shovel_task
def rank_and_plot_all_datasets(tasks_list, local_dir, base_experiment_dir,
                               metalearning_experiment_dir, output_dir,
                               file_type, figsize_x=16, figsize_y=6,
                               num_bootstrap_samples=100, cut=50, *optimizers):
    """
    Construct a ranking plot for all tasks specified in a file.

    Parameters
    ----------
        tasks_list : str
            Path of a file, every line is the path of one task file.

        local_dir : str
            Path to the directory which contains the pyMetaLearn directory
            structure.

        base_experiment_dir : str
            Path to the directory which contains base experiments (e.g. grid
            search), relative to local_dir.

        metalearning_experiment_dir : str
            Path to the directory which contains metalearning experiments to
            be plotted, relative to local_dir.

        output_dir : str
            Directory inside `local_dir/plots`, in which plots will be saved.

        file_type : str, in {'pdf', 'png'}, default='png'
            The output type of the produced plots.

        figsize_x : int, default=16
            Size of the plots x-axis in inch.

        figsize_y : int, default=6
            Size of the plots y-axis in inch.

        num_bootstrap_samples : int, default=100
            How many rankings should be averaged.

        cut : int, default=50
            Cut after that many function evaluations.

        optimizers : str
            The name of at least one hyperparameter optimization algorithm
            which is present in _base.get_all_available_optimizers().

    Examples
    ----
    Run this task on the command line:

    .. code:: bash

        shovel plotting.rankings.rank_and_plot_all_datasets
        ~/mhome/thesis/experiments/AutoSklearn/metalearning_experiments/2014_07_27_Thesis/tasks.txt
        ~/mhome/thesis/experiments/AutoSklearn base_experiments/2014_07_27_Thesis
        metalearning_experiments/2014_07_27_Thesis 2014_11_17_AAAI
        png 16 6 100 50 SMAC TPE random 'MI-SMAC(5,random,All)'
    """
    local_dir = pyMetaLearn.directory_manager.set_local_directory(local_dir)
    available_optimizers = meta_plot_util.get_all_available_optimizers()

    experiments_directory = os.path.join(local_dir, metalearning_experiment_dir)
    plot_dir = os.path.join(pyMetaLearn.directory_manager.plot_directory(),
                            output_dir)


    with open(tasks_list) as fh:
        tasks_list = fh.readlines()

    tasks = []
    for line in tasks_list:
        line = line.strip()
        tasks.append(pyMetaLearnTask.from_file(line))

    try:
        os.makedirs(plot_dir)
    except:
        pass

    cut = int(cut)
    figsize_x = int(figsize_x)
    figsize_y = int(figsize_y)
    n_jobs = -1

    rankings = []
    #for task in tasks:
    #    ranking = rank_and_plot_one_task(task.task_id, local_dir,
    #                                     base_experiment_dir,
    #                                     metalearning_experiment_dir,
    #                                     output_dir, file_type, figsize_x,
    #                                     figsize_y, num_bootstrap_samples,
    #                                     cut, *optimizers)
    #    rankings.append(ranking)

    rankings = Parallel(n_jobs=n_jobs)(delayed(rank_and_plot_one_task)
                                           (task.task_id, local_dir,
                                            base_experiment_dir,
                                            metalearning_experiment_dir,
                                            output_dir, file_type, figsize_x,
                                            figsize_y, num_bootstrap_samples,
                                            cut, *optimizers)
                                       for task in tasks)

    num_datasets = len(rankings)
    ranking = reduce(np.add, rankings)
    ranks = ranking / float(num_datasets)

    # figsize=(9, 5)
    plt.figure(dpi=600, figsize=(figsize_x, figsize_y))
    ax = plt.subplot(111)

    colors = plot_util.get_plot_colors()
    for i, optimizer in enumerate(optimizers):
        ax.plot(range(cut), np.append(np.mean(ranks), ranks[1:, i]),
                color=colors.next(),
                linewidth=3, label=optimizer.replace("\\", ""))

    ax.set_xlabel("#Function evaluations")
    ax.set_ylabel("Average rank")

    ax.legend(loc='upper center', bbox_to_anchor=(0.5, -0.05),
              # ax.legend(loc='best',# bbox_to_anchor=(0.5, -0.05),
              fancybox=True, shadow=True, ncol=3, labelspacing=0.25,
              fontsize=12)
    box = ax.get_position()

    ax.set_position([box.x0, box.y0 + box.height * 0.1,
                     box.width, box.height * 0.9])

    for plot_suffix in ['png', 'pdf']:
        plt.savefig("%s/all_datasets.%s" % (plot_dir, plot_suffix))


@shovel_task
def rank_and_plot_one_task(task_id, local_dir, base_experiment_dir,
                           metalearning_experiment_dir, output_dir,
                           file_type='png', figsize_x=16, figsize_y=6,
                           num_bootstrap_samples=100, cut=50, *optimizers):
    """
    Construct a ranking plot for a task.

    Parameters
    ----------
        task_id : int
            Unique identifier of the task which should be plotted

        local_dir : str
            Path to the directory which contains the pyMetaLearn directory
            structure.

        base_experiment_dir : str
            Path to the directory which contains base experiments (e.g. grid
            search), relative to local_dir.

        metalearning_experiment_dir : str
            Path to the directory which contains metalearning experiments to
            be plotted, relative to local_dir.

        output_dir : str
            Directory inside `local_dir/plots`, in which plots will be saved.

        file_type : str, in {'pdf', 'png'}, default='png'
            The output type of the produced plots.

        figsize_x : int, default=16
            Size of the plots x-axis in inch.

        figsize_y : int, default=6
            Size of the plots y-axis in inch.

        num_bootstrap_samples : int, default=100
            How many rankings should be averaged.

        cut : int, default=50
            Cut after that many function evaluations.

        optimizers : str
            The name of at least one hyperparameter optimization algorithm
            which is present in _base.get_all_available_optimizers().

    Examples
    ----
    Run this task on the command line:

    .. code:: bash

        shovel plotting.rankings.rank_and_plot_one_task
        ~/mhome/thesis/experiments/AutoSklearn
        base_experiments/2014_07_27_Thesis
        metalearning_experiments/2014_07_27_Thesis
        2014_11_17_AAAI PNG SMAC TPE random 'MI-SMAC(5,random,All)'
    """
    local_dir = pyMetaLearn.directory_manager.set_local_directory(local_dir)
    available_optimizers = meta_plot_util.get_all_available_optimizers()

    experiments_directory = os.path.join(local_dir, metalearning_experiment_dir)
    plot_dir = os.path.join(pyMetaLearn.directory_manager.plot_directory(),
                            output_dir)

    try:
        os.makedirs(plot_dir)
    except:
        pass

    joblib_context = Memory(cachedir=plot_dir, verbose=0)

    task_id = int(task_id)
    tasks = entities_base.get_tasks()
    task = pyMetaLearnTask.from_file(tasks[task_id])

    exp_dir = "%s/did_%d" % (experiments_directory, task.dataset_id)

    argument_list = []
    for optimizer in optimizers:
        pkls = glob.glob(available_optimizers[optimizer] % exp_dir)
        if len(pkls) > 0:
            argument_list.append(optimizer)
            argument_list.extend(pkls)
        else:
            print "Found no pkls for %s %s" % (
                optimizers[optimizer], exp_dir)

    pkl_list_main, name_list_main = \
        plot_util.get_pkl_and_name_list(argument_list)

    figsize_x = int(figsize_x)
    figsize_y = int(figsize_y)
    num_bootstrap_samples = int(num_bootstrap_samples)
    calculate_ranking = joblib_context.cache(meta_plot_util.calculate_ranking)
    ranking, optimizers_ = calculate_ranking(pkl_list_main, name_list_main,
                                             bootstrap_samples=num_bootstrap_samples,
                                             cut=cut)

    meta_plot_util.plot_ranking(ranking, optimizers_,
        save="%s/ranks_did%d.%s" % (plot_dir, task.dataset_id, file_type),
        figsize=(figsize_x, figsize_y))

    return ranking

