

def metafeature_calculation_time():
    print "Using a total of %d tasks(dataset) for the evaluation" % len(tasks)
    for idx, task in enumerate(tasks):

        ground_truth = metabase.get_runs(task.task_id)

        if not ground_truth:
            continue

        dataset = task._get_dataset()
        metafeatures, calculation_time = dataset.get_metafeatures(
            target=task.target_feature, return_times=True,
            return_helper_functions=True)
        metafeatures_calculation_time = sum(calculation_time.values())
        # Calculation times for only the training set
        t_metafeatures, t_calculation_time = dataset.get_metafeatures(
            target=task.target_feature, return_times=True,
            split_file_name=task.estimation_procedure[
                "local_test_split_file"], return_helper_functions=True)
        t_metafeatures_calculation_time = sum(t_calculation_time.values())

        times = []
        # Somehow read the calculation time of the metafeatures
        for run in ground_truth:
            times.append(run.runtime)

        print task.dataset_id, dataset._name, np.nansum(times), \
            np.nanmean(times), np.nanstd(times), np.nanmin(times), \
            np.nanmax(times), metafeatures_calculation_time, \
            metafeatures_calculation_time / np.nanmean(times), \
            t_metafeatures_calculation_time, t_metafeatures_calculation_time \
                                             / np.nanmean(times)