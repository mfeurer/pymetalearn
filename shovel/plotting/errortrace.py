from collections import defaultdict
import cPickle
import glob
import os

import numpy as np
import matplotlib.pyplot as plt

import pyMetaLearn.directory_manager
from pyMetaLearn.entities import entities_base
from pyMetaLearn.entities.task import Task as pyMetaLearnTask
from pyMetaLearn.metalearning.meta_base import MetaBase
from pyMetaLearn.utils import plot_utils as meta_plot_util

from HPOlib.Plotting import plot_util, plotTraceWithStd_perEval

from joblib import Memory, Parallel, delayed
from shovel import task as shovel_task



@shovel_task
def error_traces(experiments_list, tasks_list, local_dir,
                 base_experiment_dir, metalearning_experiment_dir,
                 output_dir, file_type='png', figsize_x=16, figsize_y=6,
                 cut=50, log=False, *optimizers):
    """
    Examples
    --------

    .. code:: bash

        shovel plotting.errortrace.error_traces
        ~/mhome/thesis/experiments/AutoSklearn/metalearning_experiments/2014_07_27_Thesis/experiments.txt
        ~/mhome/thesis/experiments/AutoSklearn/metalearning_experiments/2014_07_27_Thesis/tasks.txt
        ~/mhome/thesis/experiments/AutoSklearn base_experiments/2014_07_27_Thesis
        metalearning_experiments/2014_07_27_Thesis 2014_11_17_AAAI
        png 16 6 50 True SMAC TPE random 'MI-SMAC(5,random,All)'
    """

    n_jobs = -1

    with open(tasks_list) as fh:
        tasks_list_ = fh.readlines()

    tasks = []
    for line in tasks_list_:
        line = line.strip()
        tasks.append(pyMetaLearnTask.from_file(line))

    # TODO somehow get the lines here...
    Parallel(n_jobs=n_jobs)(delayed(error_trace_for_task)
                                   (task.task_id, experiments_list,
                                    tasks_list, local_dir,
                                    base_experiment_dir,
                                    metalearning_experiment_dir,
                                    output_dir, file_type, figsize_x,
                                    figsize_y, cut, log, *optimizers)
                                   for task in tasks)


@shovel_task
def error_trace_for_task(task_id, experiments_list, tasks_list, local_dir,
                         base_experiment_dir, metalearning_experiment_dir,
                         output_dir, file_type='png', figsize_x=16, figsize_y=6,
                         cut=50, log=False, *optimizers):
    """
    Examples
    --------

    .. code:: bash

        shovel plotting.errortrace.error_trace_for_task 232
        ~/mhome/thesis/experiments/AutoSklearn/metalearning_experiments/2014_07_27_Thesis/experiments.txt
        ~/mhome/thesis/experiments/AutoSklearn/metalearning_experiments/2014_07_27_Thesis/tasks.txt
        ~/mhome/thesis/experiments/AutoSklearn
        base_experiments/2014_07_27_Thesis metalearning_experiments/2014_07_27_Thesis 2014_11_17_AAAI
        png 16 6 50 True SMAC TPE random 'MI-SMAC(5,random,All)'
    """

    local_dir = pyMetaLearn.directory_manager.set_local_directory(local_dir)
    available_optimizers = meta_plot_util.get_all_available_optimizers()

    experiments_directory = os.path.join(local_dir, metalearning_experiment_dir)
    plot_dir = os.path.join(pyMetaLearn.directory_manager.plot_directory(),
                            output_dir)

    try:
        os.makedirs(plot_dir)
    except:
        pass

    joblib_context = Memory(cachedir=plot_dir, verbose=0)

    task_id = int(task_id)
    tasks = entities_base.get_tasks()
    task = pyMetaLearnTask.from_file(tasks[task_id])

    cut = int(cut)
    figsize_x = int(figsize_x)
    figsize_y = int(figsize_y)
    log = bool(log)

    try:
        os.makedirs(plot_dir)
    except Exception as e:
        print e

    with open(experiments_list) as fh:
        base_experiments_list = fh.readlines()
    with open(tasks_list) as fh:
        tasks_list = fh.readlines()
    metabase = MetaBase(tasks_list, base_experiments_list)

    exp_dir = os.path.join(experiments_directory,
                           "did_%d" % task.dataset_id)
    argument_list = []
    for optimizer in optimizers:
        pkls = glob.glob(available_optimizers[optimizer] % exp_dir)
        if len(pkls) > 0:
            argument_list.append(optimizer)
            argument_list.extend(pkls)
        else:
            print "Found no pkls for %s" % (available_optimizers[optimizer] % exp_dir)

    pkl_list_main, name_list_main = plot_util.\
        get_pkl_and_name_list(argument_list)

    ground_truth = metabase.get_runs(task.task_id)
    optimum = np.nanmin([run.result for run in ground_truth])

    plotTraceWithStd_perEval.main(pkl_list_main, name_list_main, True,
                                  optimum=optimum,
                                  save="%s/optimum_error_trace_did%d.%s" % (
                                      plot_dir, task.dataset_id, file_type),
                                  print_lenght_trial_list=False,
                                  log=log, cut=cut,
                                  # TODO make these possible
                                  #figsize_x=figsize_x,
                                  #figsize_y=figsize_y,
                                  markers=plot_util.get_plot_markers())
                                  #colors=itertools.cycle(colors.copy()));

    plt.close()


@shovel_task
def averaged_error_trace(experiments_list, tasks_list, local_dir,
                         base_experiment_dir, metalearning_experiment_dir,
                         output_dir, file_type='png', figsize_x=16, figsize_y=6,
                         cut=50, log=False, *optimizers):
    """
    Examples
    --------

    .. code:: bash

        shovel plotting.errortrace.averaged_error_trace
        ~/mhome/thesis/experiments/AutoSklearn/metalearning_experiments/2014_07_27_Thesis/experiments.txt
        ~/mhome/thesis/experiments/AutoSklearn/metalearning_experiments/2014_07_27_Thesis/tasks.txt
        ~/mhome/thesis/experiments/AutoSklearn base_experiments/2014_07_27_Thesis
        metalearning_experiments/2014_07_27_Thesis 2014_11_17_AAAI
        png 12 9 50 True SMAC TPE random 'MI-SMAC(5,random,All)'
    """

    local_dir = pyMetaLearn.directory_manager.set_local_directory(local_dir)
    available_optimizers = meta_plot_util.get_all_available_optimizers()

    experiments_directory = os.path.join(local_dir, metalearning_experiment_dir)
    plot_dir = os.path.join(pyMetaLearn.directory_manager.plot_directory(),
                            output_dir)

    try:
        os.makedirs(plot_dir)
    except:
        pass

    cut = int(cut)
    figsize_x = int(figsize_x)
    figsize_y = int(figsize_y)
    log = bool(log)

    gigantic_pickle_dict = {optimizer: [] for optimizer in optimizers}
    gigantic_scale_dict = {optimizer: [] for optimizer in optimizers}

    with open(experiments_list) as fh:
        base_experiments_list = fh.readlines()
    with open(tasks_list) as fh:
        tasks_list = fh.readlines()
    metabase = MetaBase(tasks_list, base_experiments_list)

    # TODO read the single error traces!
    for idx, task in enumerate(metabase.tasks):
        ground_truth = metabase.get_runs(task.task_id)
        minimum = np.nanmin([run.result for run in ground_truth])
        maximum = np.nanmax([run.result for run in ground_truth])

        exp_dir = os.path.join(experiments_directory,
                               "did_%d" % (task.dataset_id))

        argument_list = []
        for optimizer in optimizers:
            pkls = glob.glob(available_optimizers[optimizer] % exp_dir)
            if len(pkls) > 0:
                argument_list.append(optimizer)
                argument_list.extend(pkls)
            else:
                print "Found no pkls for %s" % (
                    available_optimizers[optimizer] % exp_dir)
        try:
            pkl_list_main, name_list_main = plot_util.get_pkl_and_name_list(
                argument_list)
        except ValueError as e:
            print "Value Error in dataset directory %s" % exp_dir
            print e

        for pkl_list, name in zip(pkl_list_main, name_list_main):
            gigantic_pickle_dict[name[0]].extend(pkl_list)

            for j in range(len(pkl_list)):
                gigantic_scale_dict[name[0]].append((minimum, maximum))

    gigantic_pickle_list = [gigantic_pickle_dict[optimizer] for optimizer in
                            optimizers]
    gigantic_scale_list = [gigantic_scale_dict[optimizer] for optimizer in
                           optimizers]

    plotTraceWithStd_perEval.main(gigantic_pickle_list, name_list_main, True,
                                  save="%s/all_datasets_log_error.%s" %
                                        (plot_dir, file_type),
                                  log=log, y_max=0, y_min=-1., cut=cut,
                                  markers=plot_util.get_plot_markers())
                                  # colors=itertools.cycle(colors.copy()))

    # This is copied from plotTraceWithStd_perEval to normalize the errors between zero and one
    trial_list = list()
    for i in range(len(gigantic_pickle_list)):
        trial_list.append(list())
        for pkl in gigantic_pickle_list[i]:
            if pkl in plot_util.cache:
                trials = plot_util.cache[pkl]
            else:
                fh = open(pkl, "r")
                trials = cPickle.load(fh)
                fh.close()
                plot_util.cache[pkl] = trials

            trace = plot_util.extract_trajectory(trials)
            trial_list[-1].append(np.array(trace))

    for i in range(len(trial_list)):
        max_len = 50
        for t in range(len(trial_list[i])):

            if len(trial_list[i][t]) < max_len:
                diff = max_len - len(trial_list[i][t])
                # noinspection PyUnusedLocal
                trial_list[i][t] = np.append(trial_list[i][t],
                                             [trial_list[i][t][-1] for x in
                                              range(diff)])
            minimum, maximum = gigantic_scale_list[i][t]
            # print trial_list[i][t],
            trial_list[i][t] = (trial_list[i][t] - minimum) / (
                maximum - minimum)

    plotTraceWithStd_perEval.plot_optimization_trace(trial_list,
        name_list_main, save="%s/all_datasets_norm_error.%s" %
                             (plot_dir, file_type),
        y_min=0, y_max=.1, print_lenght_trial_list=False, log=False,
        markers=plot_util.get_plot_markers())
        #colors=itertools.cycle(colors.copy()))