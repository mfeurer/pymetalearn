from functools import reduce
import glob
import os

import numpy as np
import matplotlib.pyplot as plt

import pyMetaLearn.directory_manager
from pyMetaLearn.entities import entities_base
from pyMetaLearn.entities.task import Task as pyMetaLearnTask
from pyMetaLearn.metalearning.meta_base import MetaBase
from pyMetaLearn.utils import plot_utils as meta_plot_util

from HPOlib.Plotting import plot_util

from joblib import Memory, Parallel, delayed
from shovel import task as shovel_task


@shovel_task
def paired_t(experiments_list, tasks_list, local_dir,
             base_experiment_dir, metalearning_experiment_dir,
             output_dir, file_type='png', figsize_x=16, figsize_y=6,
             cut=50, log=False, *optimizers):

    local_dir = pyMetaLearn.directory_manager.set_local_directory(local_dir)
    available_optimizers = meta_plot_util.get_all_available_optimizers()

    experiments_directory = os.path.join(local_dir, metalearning_experiment_dir)
    plot_dir = os.path.join(pyMetaLearn.directory_manager.plot_directory(),
                            output_dir)

    # Plot statistical significant wins
    try:
        os.makedirs(plot_dir)
    except Exception as e:
        pass

    with open(experiments_list) as fh:
        base_experiments_list = fh.readlines()
    with open(tasks_list) as fh:
        tasks_list = fh.readlines()
    metabase = MetaBase(tasks_list, base_experiments_list)

    trial_list_per_dataset = []
    trial_list_per_dataset2 = []
    name_list_per_dataset = []

    for idx, task in enumerate(metabase.tasks):
        exp_dir = "%s/did_%d" % (experiments_directory, task.dataset_id)

        argument_list = []
        for optimizer in optimizers:

            if optimizer in optimizers:
                pkls = glob.glob(available_optimizers[optimizer] % exp_dir)
                if len(pkls) > 0:
                    argument_list.append(optimizer)
                    argument_list.extend(pkls)
                else:
                    print "Found no pkls for %s" % (
                        available_optimizers[optimizer] % exp_dir)

        pkl_list_main, name_list_main = plot_util.get_pkl_and_name_list(
            argument_list)
        trial_list_per_dataset.append(pkl_list_main)
        name_list_per_dataset.append(name_list_main)

    meta_plot_util.plot_summed_wins_of_optimizers(
        trial_list_per_dataset, name_list_per_dataset, legend_ncols=2,
        save=("%s/percentage_of_wins_" % (plot_dir) + "%s." + file_type))